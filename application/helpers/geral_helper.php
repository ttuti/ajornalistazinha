<?php



function get_keyLangByCount($op = '', $count = '0')
{
    $result = '';
    switch ($op)
    {
        case 'item':
            $result = ($count == 1) ? "item" : "itens";
            break;

        case 'comentario':
            $result = ($count == 1) ? "comentario" : "comentarios";
            break;
        
        default:
            # code...
            break;
    }
    return $result;
}




function get_quantidade($op = '', $param = '')
{
    $CI = &get_instance();
    $result = '-1';
    switch ($op)
    {
        case 'comentarios':
            $r = $CI->db->query("SELECT COUNT(*) AS valor FROM comentarios WHERE comentario_comida = '$param'");
            $result = $r->row_array()["valor"];
            break;
        
        default:
            # code...
            break;
    }
    return $result;
}



function lista($option, $param = '')
{
    $CI = &get_instance();
    $result = array();

    switch($option)
    {
        case 'countItemsByCategory':
            $r = $CI->db->query("SELECT categorias.*, count(comidas.comida_categoria) AS valor FROM categorias
            LEFT JOIN comidas ON comidas.comida_categoria = categorias.categ_id
            GROUP BY categorias.categ_id");
            
            foreach ($r->result_array() as $key => $value)
            {
                $result[$value["categ_id"]] = $value["valor"];
            }
            break;

        case 'categorias':
            $r = $CI->db->query("SELECT categ_id, categ_nome FROM categorias");
            
            foreach ($r->result_array() as $key => $value)
            {
                $result[$value["categ_id"]] = $value["categ_nome"];
            }
            break;

        case 'food_tags':
            $r = $CI->db->query("SELECT tag_id, tag_nome, tag_color FROM tags");
            
            foreach ($r->result_array() as $key => $value)
            {
                $result[$value["tag_id"]] = $value;
            }
            break;

        case 'top10receitas':
            $r = $CI->db->query("SELECT * FROM comidas LIMIT 0, 10");
            $result = $r->result_array();
            break;

        case 'top10slides':
            $r = $CI->db->query("SELECT * FROM slides LIMIT 0, 10");
            $result = $r->result_array();
            break;

        case 'top10foodComment':
            $r = $CI->db->query("SELECT * FROM comentarios WHERE comentario_comida = '$param' LIMIT 0, 10");
            $result = $r->result_array();
            break;

        case 'random5receitas':
            $r = $CI->db->query("SELECT * FROM comidas ORDER BY RAND() LIMIT 0, 10");
            $result = $r->result_array();
            break;

        case 'top5feedbacks':
            $r = $CI->db->query("SELECT * FROM feedbacks LIMIT 0, 5");
            $result = $r->result_array();
            break;

        default:
            break;
    }

    return $result;
}

function flName($strName)
{
    $arr = explode(" ", $strName);
    return $arr[0]." ".$arr[sizeof($arr) -1];
}



?>