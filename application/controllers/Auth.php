<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model("Mdl_geral", "geral");
		$data["dados"] = array();
		$this->load->view('contactos/home', $data);
	}
	
	public function login()
	{
		$this->form("login");
	}
	
	public function register()
	{
		$this->form("register");
	}

	public function form($tipo = 'login')
	{
		$this->load->model("Mdl_geral", "geral");
        $data["dados"] = array();
        
        if($tipo == "register")
        {
            $this->load->view('register_login', $data);
		}
		else
		{
			$this->load->view('form_login', $data);
		}
	}
	
	public function logar()
	{
		$email = $_POST["user"]??"";
		$password = $_POST["pass"]??"";
		$this->load->model("Mdl_geral", "geral");
		$dados = $this->geral->get_userdata($email);

		if(empty($dados))
		{
			//USUARIO NÃO EXISTE
			redirect(base_url()."Auth/login");
		}
		else if($dados["usuario_password"] != $password)
		{
			//SENHA ERRADA
			redirect(base_url()."Auth/login");
		}
		else
		{
			session_start();
			$this->session->set_userdata($dados);
		}
		redirect(base_url());
	}
    
    public function logout()
	{
		$this -> load -> model('mdl_geral');
		if(!isset($_SESSION))
		{
			session_start();
		}
		session_destroy();
		redirect(base_url()."Home","refresh");
	}
}
