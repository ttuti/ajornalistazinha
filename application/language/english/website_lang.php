<?php

$lang['menu_home'] = 'Home';
$lang['menu_find'] = 'List';
$lang['menu_category'] = 'Categories';
$lang['menu_foodbook'] = 'Recipe\'s Book';
$lang['menu_recipes'] = 'Recipes';
$lang['menu_order'] = 'Order';
$lang['menu_contact'] = 'Contacts';


//==========================================

$lang['category'] = 'Category';
$lang['categories'] = 'Categories';
$lang['comentario'] = 'Comment';
$lang['comentarios'] = 'Comments';
$lang['recipes'] = 'Recipes';
$lang['view_more'] = 'View more';
$lang['views'] = 'Views';
$lang['item'] = 'Item';
$lang['itens'] = 'Itens';
$lang['enviar'] = 'Send';
$lang['bemvindo'] = 'Welcome';
$lang['entrar'] = 'Login';
$lang['criar_conta'] = 'SignUp';
$lang['email'] = 'E-mail';
$lang['senha'] = 'Password';
$lang['minha_conta'] = 'My account';
$lang['sair'] = 'Logout';
$lang['www_gerir'] = 'Website Admin';

//==========================================
$lang["envie_sua_consulta"] = "Send us your query anytime!";
$lang["entre_em_contacto"] = "Get in Touch";


//PAISES
$lang['angola'] = 'Angola';
$lang['portugal'] = 'Portugal';
$lang['usa'] = 'United States of America';

?>