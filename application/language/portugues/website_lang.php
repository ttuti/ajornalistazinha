<?php

$lang['menu_home'] = 'Página Inicial';
$lang['menu_find'] = 'Listar';
$lang['menu_category'] = 'Categorias';
$lang['menu_foodbook'] = 'Livro de receitas';
$lang['menu_recipes'] = 'Receitas';
$lang['menu_order'] = 'Encomendar';
$lang['menu_contact'] = 'Contactos';


//==========================================

$lang['category'] = 'Categoria';
$lang['categories'] = 'Categorias';
$lang['comentario'] = 'Comentário';
$lang['comentarios'] = 'Comentários';
$lang['recipes'] = 'Receitas';
$lang['view_more'] = 'Ver mais';
$lang['views'] = 'Visualizações';
$lang['item'] = 'Item';
$lang['itens'] = 'Itens';
$lang['enviar'] = 'Enviar';
$lang['bemvindo'] = 'Bem vindo';
$lang['entrar'] = 'Entrar';
$lang['criar_conta'] = 'Criar conta';
$lang['email'] = 'Email';
$lang['senha'] = 'Senha';
$lang['minha_conta'] = 'Minha conta';
$lang['sair'] = 'Sair';
$lang['www_gerir'] = 'Gerir Website';

//==========================================
$lang["envie_sua_consulta"] = "Consulte-nos há qualquer momento!";
$lang["entre_em_contacto"] = "Entre em contacto";


//PAISES
$lang['angola'] = 'Angola';
$lang['portugal'] = 'Portugal';
$lang['usa'] = 'Estados Unidos da America';

?>